package com.slabblecore.request;

public class CartItemUpdateRequest {

    int packageId;
    int noOfBathRoom;
    int noOfBedRoom;

    public int getPackageId() {
        return packageId;
    }

    public void setPackageId(int packageId) {
        this.packageId = packageId;
    }

    public int getNoOfBathRoom() {
        return noOfBathRoom;
    }

    public void setNoOfBathRoom(int noOfBathRoom) {
        this.noOfBathRoom = noOfBathRoom;
    }

    public int getNoOfBedRoom() {
        return noOfBedRoom;
    }

    public void setNoOfBedRoom(int noOfBedRoom) {
        this.noOfBedRoom = noOfBedRoom;
    }
}
