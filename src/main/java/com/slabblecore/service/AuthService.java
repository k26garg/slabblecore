package com.slabblecore.service;


import com.slabblecore.entity.Role;
import com.slabblecore.entity.User;
import com.slabblecore.enums.LoginType;
import com.slabblecore.enums.RoleName;
import com.slabblecore.repositories.UserRepository;
import com.slabblecore.repositories.RoleRepository;
import com.slabblecore.request.SignInRequest;
import com.slabblecore.request.SignUpRequest;
import com.slabblecore.response.BaseResponse;
import com.slabblecore.response.UserResponse;
import com.slabblecore.security.CustomUserDetailsService;
import com.slabblecore.security.JwtTokenProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import java.util.HashSet;
import java.util.Set;

@Service
public class AuthService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    CustomUserDetailsService customUserDetailsService;

    @Autowired
    JwtTokenProvider tokenProvider;

    public ResponseEntity<UserResponse> registerUserWithMobileNumber(String mobileNumber){
        User user = userRepository.findByMobileNumber(mobileNumber);

        UserResponse userResponse = new UserResponse();

        if(user==null){
            user = new User();
            user.addMetadata();
            user.setMobileNumber(mobileNumber);

            user.setLoginType(LoginType.MOBILE);

            Role userRole = roleRepository.findByName(RoleName.ROLE_USER);

            Set<Role> roles = new HashSet<Role>();
            roles.add(userRole);

            user.setRoles(roles);

            userRepository.save(user);

            userResponse.setStatus(HttpStatus.CREATED.name());
            userResponse.setStatusCode(HttpStatus.CREATED.value());
            userResponse.setMessage("Successful");

            return new ResponseEntity<UserResponse>(userResponse,HttpStatus.OK);

        }
        else{
            userResponse.setStatus(HttpStatus.BAD_REQUEST.name());
            userResponse.setStatusCode(HttpStatus.BAD_REQUEST.value());
            userResponse.setMessage("User is already exists");


            return new ResponseEntity<UserResponse>(userResponse,HttpStatus.BAD_REQUEST);
        }
    }

    public ResponseEntity<UserResponse> registerUser(String mobileNumber, SignUpRequest signUpRequest) {

        User user = null;

        if (signUpRequest != null && !StringUtils.isEmpty(signUpRequest.getEmail()) && !StringUtils.isEmpty(signUpRequest.getName()) && !StringUtils.isEmpty(signUpRequest.getImageUrl())) {

            UserResponse userResponse = new UserResponse();
            user = userRepository.findByMobileNumber(mobileNumber);


            if (user != null) {

                String loginType = signUpRequest.getLoginType();

                if(LoginType.FACEBOOK.name().equalsIgnoreCase(loginType)){
                    user.setLoginType(LoginType.FACEBOOK);
                }
                else if(LoginType.GOOGLE.name().equalsIgnoreCase(loginType)){
                    user.setLoginType(LoginType.GOOGLE);
                }
                else{
                    user.setLoginType(LoginType.MOBILE);
                }

                user.setEmail(signUpRequest.getEmail());
                user.setName(signUpRequest.getName());
                user.setImageUrl(signUpRequest.getImageUrl());

                userRepository.save(user);


                UserDetails userDetails = customUserDetailsService
                        .loadUserByUsername(String.valueOf(user.getMobileNumber()));

                String token = tokenProvider.generateToken(userDetails);


                userResponse.setMessage("Successfully created user");
                userResponse.setStatus(HttpStatus.CREATED.name());
                userResponse.setStatusCode(HttpStatus.CREATED.value());
                userResponse.setToken(token);

                return new ResponseEntity<UserResponse>(userResponse, HttpStatus.OK);

            } else {

                userResponse.setMessage("User does not exist");
                userResponse.setStatus(HttpStatus.BAD_REQUEST.name());
                userResponse.setStatusCode(HttpStatus.BAD_REQUEST.value());

                return new ResponseEntity<UserResponse>(userResponse,  HttpStatus.BAD_REQUEST);


            }

        }

        return null;
    }

    public ResponseEntity<BaseResponse> loginUser(SignInRequest signInRequest) {
        if(signInRequest!=null && !StringUtils.isEmpty(signInRequest.getMobileNumber())){
            BaseResponse baseResponse = new BaseResponse();

            User user = userRepository.findByMobileNumber(signInRequest.getMobileNumber());

            if(user!=null){
                UserDetails userDetails = customUserDetailsService
                        .loadUserByUsername(user.getMobileNumber());

                String token = tokenProvider.generateToken(userDetails);

                baseResponse.setMessage("Successfully Login ");
                baseResponse.setStatus(HttpStatus.OK.name());
                baseResponse.setStatusCode(HttpStatus.OK.value());
                baseResponse.setToken(token);

                return new ResponseEntity<BaseResponse>(baseResponse, HttpStatus.OK);
            }
            else{
                baseResponse.setMessage("User does not exist!");
                baseResponse.setStatus(HttpStatus.NOT_FOUND.name());
                baseResponse.setStatusCode(HttpStatus.NOT_FOUND.value());
                return new ResponseEntity<BaseResponse>(baseResponse, HttpStatus.NOT_FOUND);
            }

        }
        else{
            return new ResponseEntity<BaseResponse>(null, null, HttpStatus.BAD_REQUEST);
        }
    }
}
