package com.slabblecore.service;

import com.slabblecore.entity.Package;
import com.slabblecore.enums.PublishStatus;
import com.slabblecore.repositories.PackageRepository;
import com.slabblecore.repositories.ServiceRepository;
import com.slabblecore.response.GetAllPackageResponse;
import com.slabblecore.response.GetAllService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Collection;
import java.util.List;


@Service
public class BaseService {

    /**
     * The Package repository.
     */
    @Autowired
    PackageRepository packageRepository;

    /**
     * The Service repository.
     */
    @Autowired
    ServiceRepository serviceRepository;

    /**
     * The Ecommerce service.
     */
    @Autowired
    EcommerceService ecommerceService;

    /**
     * Gets all packages.
     *
     * @param status the status
     * @return the all packages
     */
    public ResponseEntity<GetAllPackageResponse> getAllPackages(String status) {

        GetAllPackageResponse getAllPackageResponse = new GetAllPackageResponse();

        if(!StringUtils.isEmpty(status)){
            PublishStatus publishStatus = PublishStatus.valueOf(status);

            List<Package> allByStatus = packageRepository.findAllByStatusOrderByPackageIdAsc(publishStatus);

            getAllPackageResponse.setPackages(allByStatus);

            return new ResponseEntity<GetAllPackageResponse>(getAllPackageResponse, HttpStatus.OK);

        }

        else{
            List<Package> allByStatus = packageRepository.findAllByStatusOrderByPackageIdAsc(PublishStatus.PUBLISH);

            getAllPackageResponse.setPackages(allByStatus);

            return new ResponseEntity<GetAllPackageResponse>(getAllPackageResponse, HttpStatus.OK);

        }
    }

    /**
     * Create package response entity.
     *
     * @param aPackage the a package
     * @return the response entity
     */
    public ResponseEntity<Package> createPackage(Package aPackage) {

        if(aPackage!=null){
            aPackage.addMetadata();

            packageRepository.save(aPackage);

            if(aPackage.isProduct()){
                ecommerceService.createProduct(aPackage);
            }

            return new ResponseEntity<Package>(aPackage,HttpStatus.CREATED);
        }
        else{
            return new ResponseEntity<>(null,HttpStatus.BAD_REQUEST);
        }

    }

    /**
     * Create service response entity.
     *
     * @param service the service
     * @return the response entity
     */
    public ResponseEntity<com.slabblecore.entity.Service> createService(com.slabblecore.entity.Service service) {

        if(service!=null){
            service.addMetadata();

            serviceRepository.save(service);

            return new ResponseEntity<com.slabblecore.entity.Service>(service,HttpStatus.CREATED);

        }
        else{
            return new ResponseEntity<>(null,HttpStatus.BAD_REQUEST);
        }

    }

    public ResponseEntity<GetAllService> getAllServiceByPackage(int packageId) {

        GetAllService getAllService = new GetAllService();
        Package aPackage = packageRepository.findByPackageId(packageId);
        Collection<com.slabblecore.entity.Service> services = aPackage.getServices();
        getAllService.setaPackage(aPackage);
        getAllService.setServices((List<com.slabblecore.entity.Service>) services);

        return new ResponseEntity<GetAllService>(getAllService,HttpStatus.OK);
    }

    public ResponseEntity<GetAllService> getAllService() {

        GetAllService getAllService = new GetAllService();
        List<com.slabblecore.entity.Service> services = serviceRepository.findAllByStatus(PublishStatus.PUBLISH);
        getAllService.setServices(services);

        return new ResponseEntity<GetAllService>(getAllService,HttpStatus.OK);
    }
}
