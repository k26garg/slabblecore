package com.slabblecore.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OtpService {

    @Autowired
    OtpGenerator otpGenerator;

    @Autowired
    SMSService smsService;

    public Boolean generateOtp(String key){

        Integer integer = otpGenerator.generateOtp(key);

        if(integer!=-1){
            String message = "Your SafeUp OTP is " + integer;
            smsService.sendSMSMessage( message,"+919610159161");
            return true;
        }
        else{
            return false;
        }



    }

    public Boolean validateOTP(String key, Integer otpNumber)
    {
        // get OTP from cache
        Integer cacheOTP = otpGenerator.getOPTByKey(key);
        if (cacheOTP.equals(otpNumber))
        {
            otpGenerator.clearOTPFromCache(key);
            return true;
        }
        return false;
    }

}
