package com.slabblecore.service;

import com.slabblecore.entity.*;
import com.slabblecore.entity.Package;
import com.slabblecore.enums.AddressType;
import com.slabblecore.repositories.*;
import com.slabblecore.request.CreateAddressRequest;
import com.slabblecore.response.AddressReponse;
import com.slabblecore.response.GetAllAddressByUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * The type Ecommerce service.
 */
@Service
public class EcommerceService {

    @Autowired
    ProductRepository productRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    AddressRepository addressRepository;

    @Autowired
    PackageRepository packageRepository;

    @Autowired
    CartRepository cartRepository;

    @Autowired
    CartOrderItemRepository cartOrderItemRepository;

    /**
     * Create product.
     *
     * @param aPackage the a package
     */
    public void createProduct(Package aPackage) {
        Product product = new Product();

        product.setName(aPackage.getName());
        product.setImageUrl(aPackage.getImageUrl());
        product.setRegularPrice(aPackage.getPrice());
        product.setSalePrice(aPackage.getPrice());
        product.setCommodityId(aPackage.getPackageId());
        product.addMetadata();

        productRepository.save(product);

    }

    public ResponseEntity<AddressReponse> createAddress(long userID, CreateAddressRequest request) {
        AddressReponse addressReponse = new AddressReponse();
        if (request != null) {
            Address address = new Address();
            address.addMetadata();

            if(AddressType.PICKUP.name().equalsIgnoreCase(request.getType())){
                address.setAddressType(AddressType.PICKUP);
            }
            else if(AddressType.PICKUP.name().equalsIgnoreCase(request.getType())){
                address.setAddressType(AddressType.DELIVERY);
            }else{
                address.setAddressType(AddressType.CURRENT);
            }

            address.setFlatNo(request.getFlatNo());
            address.setLandmark(request.getLandmark());
            address.setLatitude(request.getLatitude());
            address.setLongitude(request.getLatitude());
            address.setAddressText(request.getAddressText());
            address.setCity(request.getCity());
            address.setState(request.getState());
            address.setPincode(request.getPincode());
            address.setAddress(request.getFlatNo()+", " + request.getLandmark() + ", "+request.getAddressText()+", " + request.getCity() + ", " + request.getState() +", " + request.getPincode());
            User user =  userRepository.findById(userID);
            address.setUser(user);
            Address response = addressRepository.save(address);

            addressReponse.setAddressId(response.getAddressId());
            addressReponse.setText(response.getAddress());

            return new ResponseEntity<AddressReponse>(addressReponse, HttpStatus.CREATED);

        } else {
            return new ResponseEntity<AddressReponse>(addressReponse, HttpStatus.BAD_REQUEST);
        }
    }

    public ResponseEntity<GetAllAddressByUser> getAllAddressByUser(long userID) {
        User user = userRepository.findById(userID);
        List<Address> addresses = addressRepository.findByUser(user);

        GetAllAddressByUser getAllAddressByUser = new GetAllAddressByUser();

        List<AddressReponse> addressList = new ArrayList<>();

        addresses.stream().forEach(address -> {
            AddressReponse addressReponse = new AddressReponse();
            addressReponse.setAddressId(address.getAddressId());
            addressReponse.setText(address.getAddress());
            addressReponse.setType(address.getAddressType().name());

            addressList.add(addressReponse);
        });

        getAllAddressByUser.setAddresses(addressList);

        return new ResponseEntity<GetAllAddressByUser>(getAllAddressByUser,HttpStatus.OK);


    }

    public Cart updateCart(long userID, int packageId, int noOfBathRoom, int noOfBedRoom) {
        User user = userRepository.findById(userID);
        Product product = productRepository.findByCommodityId(packageId);
        Package aPackage = packageRepository.findByPackageId(packageId);
        Cart cart = cartRepository.findByUser(user);

        if(cart==null){
            cart = new Cart();
            cart.addMetadata();
            cart.setUser(user);
            cartRepository.saveAndFlush(cart);
        }

        CartOrderItem cartOrderItem = cartOrderItemRepository.findByCart(cart);

        Set<CartOrderItem> items = cart.getItems();
        if(cartOrderItem==null){
            cartOrderItem = new CartOrderItem();
            items = new HashSet<>();
        }

        double totalAmount = aPackage.getPrice();

        cartOrderItem.setNo_of_bathroom(aPackage.getMinBathRomCount());
        cartOrderItem.setNo_of_bedroom(aPackage.getMinBedRoomCount());

        if(noOfBathRoom==aPackage.getMinBathRomCount() || noOfBathRoom>aPackage.getMinBathRomCount()){
            cartOrderItem.setNo_of_bathroom(noOfBathRoom);
            double bathRoomPrice = aPackage.getBathRoomPerPrice()*(noOfBathRoom-1);
            totalAmount = totalAmount + bathRoomPrice;

        }

        if(noOfBedRoom==aPackage.getMinBedRoomCount() || noOfBedRoom>aPackage.getMinBedRoomCount()){
            cartOrderItem.setNo_of_bedroom(noOfBedRoom);
            double bedRoomPrice  = aPackage.getBedRoomPerPrice()*(noOfBedRoom-1);
            totalAmount = totalAmount + bedRoomPrice;
        }



        cartOrderItem.setRegularAmount(aPackage.getPrice());
        cartOrderItem.setTotalAmount(totalAmount);
        cartOrderItem.setProduct(product);
        cartOrderItem.setCart(cart);


        items.add(cartOrderItem);

        cart.setItems(items);

        cartOrderItemRepository.saveAndFlush(cartOrderItem);

       return cartRepository.findByUser(user);

    }
}
