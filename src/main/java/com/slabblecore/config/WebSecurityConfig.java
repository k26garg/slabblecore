package com.slabblecore.config;

import com.slabblecore.security.CustomUserDetailsService;
import com.slabblecore.security.JwtAuthenticationEntryPoint;
import com.slabblecore.security.JwtAuthenticationFilter;
import com.slabblecore.security.JwtTokenProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.firewall.DefaultHttpFirewall;
import org.springframework.security.web.firewall.HttpFirewall;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	JwtTokenProvider jwtTokenProvider;

	@Autowired
	private JwtAuthenticationEntryPoint unauthorizedHandler;


	@Autowired
	private CustomUserDetailsService customUserDetailsService;

	@Value("${jwt.header}")
	private String tokenHeader;

	@Value("${jwt.route.authentication.path}")
	private String authenticationPath;

	private static final String[] SECURE_ADMIN_PATTERNS = { "/admin", "/admin/*", "/admin/*/**", "/admin/*/**/**"};


	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(customUserDetailsService).passwordEncoder(passwordEncoderBean());
	}

	@Bean
	public PasswordEncoder passwordEncoderBean() {
		return new BCryptPasswordEncoder();
	}

	@Bean
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}

	@Override
	protected void configure(HttpSecurity httpSecurity) throws Exception {
		httpSecurity.csrf().disable().exceptionHandling().authenticationEntryPoint(unauthorizedHandler).and()
				.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and().authorizeRequests()
				.antMatchers(HttpMethod.GET, "/").permitAll()
				.antMatchers(HttpMethod.POST, "/api/v1/auth/signup").permitAll()
				.antMatchers(HttpMethod.POST, "/api/v1/auth/signin").permitAll()
				.antMatchers(HttpMethod.GET, "/api/v1/auth/*").permitAll()
				.antMatchers(HttpMethod.GET, "/api/v1/auth/*/**").permitAll()
				.antMatchers(HttpMethod.GET, "/api/v1/auth/*/**/**").permitAll()
				.antMatchers(HttpMethod.POST, "/api/v1/admin/*").access("hasRole('ROLE_ADMIN')")
				.antMatchers(HttpMethod.POST,"/api/v1/user/*").access("hasRole('ROLE_USER')")
				.antMatchers(HttpMethod.GET,"/api/v1/user/*").access("hasRole('ROLE_USER')");


		httpSecurity.authorizeRequests().antMatchers("/resources/**").permitAll().anyRequest().permitAll();

		// Custom JWT based security filter

		JwtAuthenticationFilter authenticationTokenFilter = new JwtAuthenticationFilter(jwtTokenProvider,customUserDetailsService);
		httpSecurity.addFilterBefore(authenticationTokenFilter, UsernamePasswordAuthenticationFilter.class);
		CaseInsensitiveRequestFilter caseInsensitiveRequestFilter = new CaseInsensitiveRequestFilter();
		httpSecurity.addFilterAfter(caseInsensitiveRequestFilter, JwtAuthenticationFilter.class);
		httpSecurity.headers().cacheControl();
	}

	@Override
	public void configure(WebSecurity web) throws Exception {
		// AuthenticationTokenFilter will ignore the below paths
		web.ignoring().antMatchers(HttpMethod.POST, authenticationPath)

				// allow anonymous resource requests
				.and().ignoring().antMatchers(HttpMethod.GET, "/", "/*.html", "**/favicon.ico", "/**/*.html",
				"/**/*.css", "/**/*.png", "/**/*.jpg", "/**/*.jpeg", "/**/*.svg", "/**/*.woff", "/**/*.ttf",
				"/**/*.js");

	}

	@Bean
	public HttpFirewall defaultHttpFirewall() {
		return new DefaultHttpFirewall();
	}
}
