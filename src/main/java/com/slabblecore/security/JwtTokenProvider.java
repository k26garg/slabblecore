package com.slabblecore.security;


import io.jsonwebtoken.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Component
public class JwtTokenProvider {

  private static final Logger logger = LoggerFactory.getLogger(JwtTokenProvider.class);

  private static final String CLAIM_KEY_USERNAME = "sub";
  private static final String CLAIM_KEY_ID = "id";
  private static final String CLAIM_KEY_NAME = "name";
  private static final String CLAIM_KEY_ROLE = "role";
  private static final String CLAIM_KEY_EMAIL_ID = "email";
  private static final String CLAIM_KEY_MOBILE = "mobile";
  private static final String CLAIM_KEY_IMAGE_URL ="imageUrl";
  private static final String CLAIM_KEY_LOGIN_TYPE ="loginType";

  private static final long serialVersionUID = -3301605591108950415L;


  @Value("${app.jwtSecret}")
  private String jwtSecret;

  @Value("${app.jwtExpirationInMs}")
  private int jwtExpirationInMs;

  public String generateToken(UserDetails userDetails) {
    Map<String, Object> claims = new HashMap<>();
    UserPrincipal userPrincipal = (UserPrincipal) userDetails;
    if (userPrincipal != null) {
      claims.put(CLAIM_KEY_ID, userPrincipal.getId());
      claims.put(CLAIM_KEY_NAME, ((UserPrincipal) userDetails).getName());
      claims.put(CLAIM_KEY_EMAIL_ID, userPrincipal.getEmail());
      claims.put(CLAIM_KEY_MOBILE,userPrincipal.getMobileNumber());
      claims.put(CLAIM_KEY_ROLE,userPrincipal.getAuthorities());
      claims.put(CLAIM_KEY_USERNAME,userPrincipal.getMobileNumber());
      claims.put(CLAIM_KEY_IMAGE_URL,userPrincipal.getImageUrl());
      claims.put(CLAIM_KEY_LOGIN_TYPE,userPrincipal.getLoginType());
    }
    return doGenerateToken(claims, userDetails.getUsername());
  }

  private String doGenerateToken(Map<String, Object> claims, String subject) {
    Date createdDate = new Date();
    Date expirationDate = new Date(createdDate.getTime() + jwtExpirationInMs);

    try {

      String compact = Jwts.builder().setClaims(claims).setSubject(subject).setIssuedAt(createdDate)
          .setExpiration(expirationDate).signWith(SignatureAlgorithm.HS256, jwtSecret).compact();
    }catch (Exception e){
      e.getStackTrace();
      e.printStackTrace();
    }

    return Jwts.builder().setClaims(claims).setSubject(subject).setIssuedAt(createdDate)
        .setExpiration(expirationDate).signWith(SignatureAlgorithm.HS256, jwtSecret).compact();
  }


  public String getClaimKeyUsernameFromJWT(String token) {
    Claims claims = Jwts.parser()
        .setSigningKey(jwtSecret)
        .parseClaimsJws(token)
        .getBody();

    return claims.get(CLAIM_KEY_MOBILE).toString();
  }

  public boolean validateToken(String authToken) {
    try {
      Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(authToken);
      return true;
    } catch (SignatureException ex) {
      logger.error("Invalid JWT signature");
    } catch (MalformedJwtException ex) {
      logger.error("Invalid JWT token");
    } catch (ExpiredJwtException ex) {
      logger.error("Expired JWT token");
    } catch (UnsupportedJwtException ex) {
      logger.error("Unsupported JWT token");
    } catch (IllegalArgumentException ex) {
      logger.error("JWT claims string is empty.");
    }
    return false;
  }

  public long getLoggedInUserID() {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    if(auth.getPrincipal().getClass()==String.class){
      return 0;
    }
    UserPrincipal loggedInUser = (UserPrincipal) (auth.getPrincipal());
    return loggedInUser.getId();
  }
}

