package com.slabblecore.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Entity
@Table(name = "order_items")
public class OrderItem {

    @Id
    @Column(name = "order_item_id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "order_item_seq")
    @SequenceGenerator(name = "order_item_seq", sequenceName = "order_item_seq", initialValue = 100)
    private long orderItemId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "product_id")
    Product product;

    @JsonBackReference(value = "orderReference")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "order_id")
    Order order;

    @Column(name = "no_of_bathromm",columnDefinition = "int default 0")
    int no_of_bathromm;

    @Column(name = "no_of_bedroom",columnDefinition = "int default 0")
    int no_of_bedroom;

    @Column(name = "price",columnDefinition = "int default 0")
    double price;

    @Column(name = "sub_total",columnDefinition = "int default 0")
    double subtotal;

    @Column(name = "payable_amount",columnDefinition = "int default 0")
    double payableAmount;

    @Column(name="regular_amount",columnDefinition = "int default 0")
    double regularAmount;

    public long getOrderItemId() {
        return orderItemId;
    }

    public void setOrderItemId(long orderItemId) {
        this.orderItemId = orderItemId;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public int getNo_of_bathromm() {
        return no_of_bathromm;
    }

    public void setNo_of_bathromm(int no_of_bathromm) {
        this.no_of_bathromm = no_of_bathromm;
    }

    public int getNo_of_bedroom() {
        return no_of_bedroom;
    }

    public void setNo_of_bedroom(int no_of_bedroom) {
        this.no_of_bedroom = no_of_bedroom;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(double subtotal) {
        this.subtotal = subtotal;
    }

    public double getPayableAmount() {
        return payableAmount;
    }

    public void setPayableAmount(double payableAmount) {
        this.payableAmount = payableAmount;
    }

    public double getRegularAmount() {
        return regularAmount;
    }

    public void setRegularAmount(double regularAmount) {
        this.regularAmount = regularAmount;
    }
}
