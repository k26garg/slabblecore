package com.slabblecore.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

@Entity
@Table(name = "user_package_linkage")
public class UserPackageLinkage extends BaseEntity{

    @Id
    @Column(name = "linkage_id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_package_linkage_seq")
    @SequenceGenerator(name = "user_package_linkage_seq", sequenceName = "user_package_linkage_seq",
            initialValue = 10000)
    int linkageId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    @JsonIgnoreProperties(value = "userPackageLinkages")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "package_id")
    private Package aPackage;

    public int getLinkageId() {
        return linkageId;
    }

    public void setLinkageId(int linkageId) {
        this.linkageId = linkageId;
    }



    public Package getaPackage() {
        return aPackage;
    }

    public void setaPackage(Package aPackage) {
        this.aPackage = aPackage;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "UserPackageLinkage{" +
                "linkageId=" + linkageId +
                ", user=" + user +
                ", aPackage=" + aPackage +
                '}';
    }
}
