package com.slabblecore.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;

@Entity
@Table(name = "CartItem")
public class CartOrderItem extends BaseEntity {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "cart_item_seq")
    @SequenceGenerator(name = "cart_item_seq", sequenceName = "cart_item_seq", initialValue = 1000)
    private long cartItemId;

    @JsonBackReference(value = "cartReference")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cart_id")
    Cart cart;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "product_id")
    Product product;

    @Column(name = "no_of_bathroom",columnDefinition = "int default 0.0")
    private int no_of_bathroom;

    @Column(name = "no_of_bedroom",columnDefinition = "int default 0.0")
    private int no_of_bedroom;

    /**
     * totalAmount = regularAmount + no_of_bathroom * its price + no_of_bedroom * its price;
     */
    @Column(name = "total_amount",columnDefinition = "int default 0.0")
    double totalAmount;

    @Column(name = "payable_amount",columnDefinition = "int default 0.0")
    double payableAmount;

    @Column(name="regular_amount",columnDefinition = "int default 0.0")
    double regularAmount;

    public long getCartItemId() {
        return cartItemId;
    }

    public void setCartItemId(long cartItemId) {
        this.cartItemId = cartItemId;
    }

    public Cart getCart() {
        return cart;
    }

    public void setCart(Cart cart) {
        this.cart = cart;
    }

    public double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public double getPayableAmount() {
        return payableAmount;
    }

    public void setPayableAmount(double payableAmount) {
        this.payableAmount = payableAmount;
    }

    public double getRegularAmount() {
        return regularAmount;
    }

    public void setRegularAmount(double regularAmount) {
        this.regularAmount = regularAmount;
    }

    public int getNo_of_bathroom() {
        return no_of_bathroom;
    }

    public void setNo_of_bathroom(int no_of_bathroom) {
        this.no_of_bathroom = no_of_bathroom;
    }

    public int getNo_of_bedroom() {
        return no_of_bedroom;
    }

    public void setNo_of_bedroom(int no_of_bedroom) {
        this.no_of_bedroom = no_of_bedroom;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
}
