package com.slabblecore.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

@Entity
@Table(name = "user_service_linkage")
public class UserServiceLinkage {

    @Id
    @Column(name = "linkage_id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_service_linkage_seq")
    @SequenceGenerator(name = "user_service_linkage_seq", sequenceName = "user_service_linkage_seq",
            initialValue = 10000)
    int linkageId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    @JsonIgnoreProperties(value = "userServiceLinkages")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "service_id")
    private Service service;

    public int getLinkageId() {
        return linkageId;
    }

    public void setLinkageId(int linkageId) {
        this.linkageId = linkageId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }


}
