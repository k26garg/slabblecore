package com.slabblecore.entity;

import javax.persistence.*;

/**
 * The type Product.
 */
@Entity
@Table(name = "Product")
public class Product extends BaseEntity {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "product_seq")
    @SequenceGenerator(name = "product_seq", sequenceName = "product_seq", allocationSize = 1000)
    private int productId;

    @Column(name = "name")
    private String name;

    /**
     * The Commodity id.
     */
    @Column(name = "commodity_id")
    int commodityId;

    /**
     * The Image url.
     */
    @Column(name = "image_url")
    String imageUrl;

    /**
     * The Sale price.
     */
    @Column(name = "sale_price",columnDefinition = "int default 0")
    double salePrice;

    /**
     * The Regular price.
     */
    @Column(name = "regular_price",columnDefinition = "int default 0")
    double regularPrice;

    /**
     * Gets product id.
     *
     * @return the product id
     */
    public int getProductId() {
        return productId;
    }

    /**
     * Sets product id.
     *
     * @param productId the product id
     */
    public void setProductId(int productId) {
        this.productId = productId;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets commodity id.
     *
     * @return the commodity id
     */
    public int getCommodityId() {
        return commodityId;
    }

    /**
     * Sets commodity id.
     *
     * @param commodityId the commodity id
     */
    public void setCommodityId(int commodityId) {
        this.commodityId = commodityId;
    }

    /**
     * Gets image url.
     *
     * @return the image url
     */
    public String getImageUrl() {
        return imageUrl;
    }

    /**
     * Sets image url.
     *
     * @param imageUrl the image url
     */
    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    /**
     * Gets sale price.
     *
     * @return the sale price
     */
    public double getSalePrice() {
        return salePrice;
    }

    /**
     * Sets sale price.
     *
     * @param salePrice the sale price
     */
    public void setSalePrice(double salePrice) {
        this.salePrice = salePrice;
    }

    /**
     * Gets regular price.
     *
     * @return the regular price
     */
    public double getRegularPrice() {
        return regularPrice;
    }

    /**
     * Sets regular price.
     *
     * @param regularPrice the regular price
     */
    public void setRegularPrice(double regularPrice) {
        this.regularPrice = regularPrice;
    }
}
