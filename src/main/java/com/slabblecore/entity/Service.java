package com.slabblecore.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.slabblecore.enums.PublishStatus;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "Service")
@JsonIgnoreProperties({"packages"})
public class Service extends BaseEntity {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "service_seq")
    @SequenceGenerator(name = "service_seq", sequenceName = "service_seq", allocationSize = 10000)
    private Long serviceId;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "image")
    private String image;

    @JsonIgnoreProperties("services")
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "package_service",
            joinColumns = {@JoinColumn(name = "service_id", referencedColumnName = "ID",table = "service")},
            inverseJoinColumns = {@JoinColumn(name = "package_id", referencedColumnName = "ID",table = "package")})
    private List<Package> packages;

    @Column(name = "publish_status")
    @NotNull
    @Enumerated(EnumType.STRING)
    private PublishStatus status;

    @JsonIgnore
    @OneToMany(mappedBy = "service", fetch = FetchType.LAZY)
    private Set<UserServiceLinkage> userServiceLinkages;

    public Long getServiceId() {
        return serviceId;
    }

    public void setServiceId(Long serviceId) {
        this.serviceId = serviceId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public List<Package> getPackages() {
        return packages;
    }

    public void setPackages(List<Package> packages) {
        this.packages = packages;
    }

    public PublishStatus getStatus() {
        return status;
    }

    public void setStatus(PublishStatus status) {
        this.status = status;
    }

    public Set<UserServiceLinkage> getUserServiceLinkages() {
        return userServiceLinkages;
    }

    public void setUserServiceLinkages(Set<UserServiceLinkage> userServiceLinkages) {
        this.userServiceLinkages = userServiceLinkages;
    }

    @Override
    public String toString() {
        return "Service{" +
                "serviceId=" + serviceId +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", image='" + image + '\'' +
                ", packages=" + packages +
                '}';
    }
}
