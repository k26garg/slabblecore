package com.slabblecore.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.Collection;
import java.util.Set;

@Entity
@Table(name = "package_extra_info")
public class PackageExtraInfo extends BaseEntity{

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "package_extra_info_seq")
    @SequenceGenerator(name = "package_seq", sequenceName = "package_seq", allocationSize = 1)
    private Long packageExtraInfo;

    @Column(name = "topic")
    private String topic;

    @Column(name = "text")
    private String text;

    @Column(name = "extra_price",columnDefinition = "int default 0.0")
    private double extraPrice;

    @JsonIgnoreProperties("packageExtraInfo")
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "package_extra_info_linkage",
            joinColumns = {@JoinColumn(name = "package_id", referencedColumnName = "ID")},
            inverseJoinColumns = {@JoinColumn(name = "package_extra_info_id", referencedColumnName = "ID")})
    private Collection<Package> packages;


    public Long getPackageExtraInfo() {
        return packageExtraInfo;
    }

    public void setPackageExtraInfo(Long packageExtraInfo) {
        this.packageExtraInfo = packageExtraInfo;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public double getExtraPrice() {
        return extraPrice;
    }

    public void setExtraPrice(double extraPrice) {
        this.extraPrice = extraPrice;
    }

    public Collection<Package> getPackages() {
        return packages;
    }

    public void setPackages(Collection<Package> packages) {
        this.packages = packages;
    }

    @Override
    public String toString() {
        return "PackageExtraInfo{" +
                "packageExtraInfo=" + packageExtraInfo +
                ", topic='" + topic + '\'' +
                ", text='" + text + '\'' +
                ", extraPrice=" + extraPrice +
                '}';
    }
}
