package com.slabblecore.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.slabblecore.enums.PublishStatus;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * The type Package.
 */
@Entity
@Table(name = "Package")
@JsonIgnoreProperties({ "services", "packageExtraInfo","userPackageLinkages" })
public class Package extends BaseEntity{

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "package_seq")
    @SequenceGenerator(name = "package_seq", sequenceName = "package_seq", allocationSize = 100)
    private int packageId;

    @Column(name = "name")
    private String name;

    @Column(name = "price",columnDefinition = "int default 0.0")
    private double price;

    @Column(name = "image_url")
    private String imageUrl;

    @JsonProperty
    @Column(name = "is_product")
    private boolean product;

    @JsonIgnoreProperties("packages")
    @ManyToMany(mappedBy = "packages", fetch = FetchType.LAZY)
    private List<Service> services;

    @JsonIgnoreProperties("packages")
    @ManyToMany(mappedBy = "packages", fetch = FetchType.LAZY)
    private List<PackageExtraInfo> packageExtraInfo;

    @Column(name = "publish_status")
    @NotNull
    @Enumerated(EnumType.STRING)
    private PublishStatus status;

    /**
     * The User package linkages.
     */
    @JsonIgnore
    @JsonIgnoreProperties(value = "aPackage")
    @OneToMany(mappedBy = "aPackage", fetch = FetchType.LAZY)
    Set<UserPackageLinkage> userPackageLinkages;

    @Column(name = "min_bathroom_count",columnDefinition = "int default 0.0")
    private int minBathRomCount;

    @Column(name = "min_bedroom_count",columnDefinition = "int default 0.0")
    private int minBedRoomCount;

    @Column(name = "bathroom_per_price",columnDefinition = "int default 0.0")
    private int bathRoomPerPrice;

    @Column(name = "bedroom_per_price",columnDefinition = "int default 0.0")
    private int bedRoomPerPrice;

    /**
     * Gets package id.
     *
     * @return the package id
     */
    public int getPackageId() {
        return packageId;
    }

    /**
     * Sets package id.
     *
     * @param packageId the package id
     */
    public void setPackageId(int packageId) {
        this.packageId = packageId;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets price.
     *
     * @return the price
     */
    public double getPrice() {
        return price;
    }

    /**
     * Sets price.
     *
     * @param price the price
     */
    public void setPrice(double price) {
        this.price = price;
    }

    /**
     * Gets services.
     *
     * @return the services
     */
    public Collection<Service> getServices() {
        return services;
    }

    /**
     * Sets services.
     *
     * @param services the services
     */
    public void setServices(List<Service> services) {
        this.services = services;
    }

    /**
     * Gets package extra info.
     *
     * @return the package extra info
     */
    public List<PackageExtraInfo> getPackageExtraInfo() {
        return packageExtraInfo;
    }

    /**
     * Sets package extra info.
     *
     * @param packageExtraInfo the package extra info
     */
    public void setPackageExtraInfo(List<PackageExtraInfo> packageExtraInfo) {
        this.packageExtraInfo = packageExtraInfo;
    }

    /**
     * Gets status.
     *
     * @return the status
     */
    public PublishStatus getStatus() {
        return status;
    }

    /**
     * Sets status.
     *
     * @param status the status
     */
    public void setStatus(PublishStatus status) {
        this.status = status;
    }

    public Set<UserPackageLinkage> getUserPackageLinkages() {
        return userPackageLinkages;
    }

    public void setUserPackageLinkages(Set<UserPackageLinkage> userPackageLinkages) {
        this.userPackageLinkages = userPackageLinkages;
    }




    public boolean isProduct() {
        return product;
    }

    public void setProduct(boolean product) {
        this.product = product;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public int getMinBathRomCount() {
        return minBathRomCount;
    }

    public void setMinBathRomCount(int minBathRomCount) {
        this.minBathRomCount = minBathRomCount;
    }

    public int getMinBedRoomCount() {
        return minBedRoomCount;
    }

    public void setMinBedRoomCount(int minBedRoomCount) {
        this.minBedRoomCount = minBedRoomCount;
    }

    public int getBathRoomPerPrice() {
        return bathRoomPerPrice;
    }

    public void setBathRoomPerPrice(int bathRoomPerPrice) {
        this.bathRoomPerPrice = bathRoomPerPrice;
    }

    public int getBedRoomPerPrice() {
        return bedRoomPerPrice;
    }

    public void setBedRoomPerPrice(int bedRoomPerPrice) {
        this.bedRoomPerPrice = bedRoomPerPrice;
    }


}
