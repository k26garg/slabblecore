package com.slabblecore.entity;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.slabblecore.enums.AddressType;
import com.sun.istack.NotNull;

import javax.persistence.*;

@Entity
@Table(name = "Address")
public class Address extends BaseEntity {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "address_seq")
    @SequenceGenerator(name = "address_seq", sequenceName = "address_seq", initialValue = 3000)
    private Long addressId;

    @Column(name = "flat_no")
    private String flatNo;

    @Column(name = "landmark")
    private String landmark;

    @Column(name = "latitude")
    private String latitude;

    @Column(name = "longitude")
    private String longitude;

    @Column(name = "address_type")
    @NotNull
    @Enumerated(EnumType.STRING)
    private AddressType addressType;


    @Column(name = "address_text")
    String addressText;

    @Column(name = "city")
    String city;

    @Column(name = "state")
    String state;

    @Column(name = "pincode")
    String pincode;


    @Column(name = "address",columnDefinition = "TEXT")
    String address;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    User user;

    public Address() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @return the addressText
     */
    public String getAddressText() {
        return addressText;
    }

    /**
     * @param addressText the addressText to set
     */
    public void setAddressText(String addressText) {
        this.addressText = addressText;
    }

    /**
     * @return the city
     */
    public String getCity() {
        return city;
    }

    /**
     * @param city the city to set
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * @return the state
     */
    public String getState() {
        return state;
    }

    /**
     * @param state the state to set
     */
    public void setState(String state) {
        this.state = state;
    }

    /**
     * @return the pincode
     */
    public String getPincode() {
        return pincode;
    }

    /**
     * @param pincode the pincode to set
     */
    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    /**
     * @return the addressId
     */
    public Long getAddressId() {
        return addressId;
    }

    /**
     * @param addressId the addressId to set
     */
    public void setAddressId(Long addressId) {
        this.addressId = addressId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getFlatNo() {
        return flatNo;
    }

    public void setFlatNo(String flatNo) {
        this.flatNo = flatNo;
    }

    public String getLandmark() {
        return landmark;
    }

    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public AddressType getAddressType() {
        return addressType;
    }

    public void setAddressType(AddressType addressType) {
        this.addressType = addressType;
    }
}

