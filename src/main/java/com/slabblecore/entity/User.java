package com.slabblecore.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.slabblecore.enums.LoginType;
import com.slabblecore.enums.RoleName;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "user_info")
public class User extends BaseEntity {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_seq")
    @SequenceGenerator(name = "user_seq", sequenceName = "user_seq", allocationSize = 1000)
    private Long userId;

    @Column(name = "email", unique = true)
    private String email;

    @Column(name = "name")
    private String name;

    @Column(name = "image_url")
    private String imageUrl;

    @Column(name = "login_type")
    @NotNull
    @Enumerated(EnumType.STRING)
    private LoginType loginType;

    @Column(name = "mobile_number", nullable = false)
    private String mobileNumber;

    @JsonIgnoreProperties("users")
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "user_role",
          joinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "ID")},
          inverseJoinColumns = {@JoinColumn(name = "ruser_id", referencedColumnName = "ID")})
    private Collection<Role> roles;

    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
    private Set<UserPackageLinkage> userPackageLinkages;

    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
    private Set<Address> addresses;

    @JsonIgnore
    @JsonIgnoreProperties(value = "user")
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
    private Set<UserServiceLinkage> userServiceLinkages;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String  mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public Collection<Role> getRoles() {
        return roles;
    }

    public void setRoles(Collection<Role> roles) {
        this.roles = roles;
    }

    public Set<UserPackageLinkage> getUserPackageLinkages() {
        return userPackageLinkages;
    }

    public void setUserPackageLinkages(Set<UserPackageLinkage> userPackageLinkages) {
        this.userPackageLinkages = userPackageLinkages;
    }

    public Set<UserServiceLinkage> getUserServiceLinkages() {
        return userServiceLinkages;
    }

    public void setUserServiceLinkages(Set<UserServiceLinkage> userServiceLinkages) {
        this.userServiceLinkages = userServiceLinkages;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public LoginType getLoginType() {
        return loginType;
    }

    public void setLoginType(LoginType loginType) {
        this.loginType = loginType;
    }

    @Override
    public String toString() {
        return "User{" +
                "userId=" + userId +
                ", email='" + email + '\'' +
                ", name='" + name + '\'' +
                ", mobileNumber=" + mobileNumber +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;
        User user = (User) o;
        return getMobileNumber() == user.getMobileNumber() &&
                getUserId().equals(user.getUserId()) &&
                getEmail().equals(user.getEmail());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getUserId());
    }

    public Set<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(Set<Address> addresses) {
        this.addresses = addresses;
    }
}
