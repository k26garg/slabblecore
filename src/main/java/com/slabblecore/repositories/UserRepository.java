package com.slabblecore.repositories;


import com.slabblecore.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User,Long> {

  User findById(long id);

  User findByEmail(String email);

  User findByMobileNumber(String mobileNumber);


}
