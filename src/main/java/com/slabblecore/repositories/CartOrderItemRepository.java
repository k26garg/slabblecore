package com.slabblecore.repositories;

import com.slabblecore.entity.Cart;
import com.slabblecore.entity.CartOrderItem;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CartOrderItemRepository extends JpaRepository<CartOrderItem,Long> {

    CartOrderItem findByCart(Cart c);
}
