package com.slabblecore.repositories;


import com.slabblecore.entity.Package;
import com.slabblecore.enums.PublishStatus;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PackageRepository extends JpaRepository<Package,Long> {

    List<Package> findAllByStatusOrderByPackageIdAsc(PublishStatus status);

    Package findByPackageId(int packageId);
}
