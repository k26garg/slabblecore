package com.slabblecore.repositories;

import com.slabblecore.entity.Package;
import com.slabblecore.entity.Service;
import com.slabblecore.enums.PublishStatus;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ServiceRepository extends JpaRepository<Service,Long> {

    List<Service> findAllByStatus(PublishStatus status);

}
