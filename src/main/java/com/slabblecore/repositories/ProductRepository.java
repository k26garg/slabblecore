package com.slabblecore.repositories;


import com.slabblecore.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product,Long> {

    Product findByProductId(long id);

    Product findByCommodityId(int commodityId);
}
