package com.slabblecore.repositories;


import com.slabblecore.entity.Cart;
import com.slabblecore.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CartRepository extends JpaRepository<Cart,Long> {

    Cart findByCartId(int cartId);

    Cart findByUser(User user);

}
