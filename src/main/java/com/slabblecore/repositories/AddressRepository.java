package com.slabblecore.repositories;


import com.slabblecore.entity.Address;

import com.slabblecore.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;
import java.util.List;

public interface AddressRepository extends JpaRepository<Address,Long> {

    List<Address> findByUser(User user);

}
