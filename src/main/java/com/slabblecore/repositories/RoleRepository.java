package com.slabblecore.repositories;

import com.slabblecore.entity.Role;
import com.slabblecore.enums.RoleName;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role,Integer> {

    Role findByName(RoleName name);
}
