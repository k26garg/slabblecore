package com.slabblecore.enums;

public enum PublishStatus {
    PUBLISH("publish"),DRAFT("draft"),TRASH("trash");

    String jsonValue;

    PublishStatus(String jsonValue) {
        this.jsonValue = jsonValue;
    }
}
