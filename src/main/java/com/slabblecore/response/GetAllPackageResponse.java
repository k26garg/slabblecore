package com.slabblecore.response;

import com.slabblecore.entity.Package;

import java.util.List;

public class GetAllPackageResponse {

    List<Package> packages;

    public List<Package> getPackages() {
        return packages;
    }

    public void setPackages(List<Package> packages) {
        this.packages = packages;
    }
}
