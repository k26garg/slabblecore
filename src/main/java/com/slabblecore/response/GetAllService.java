package com.slabblecore.response;


import com.slabblecore.entity.Package;
import com.slabblecore.entity.Service;

import java.util.List;

public class GetAllService {

    List<Service> services;
    Package aPackage;

    public Package getaPackage() {
        return aPackage;
    }

    public void setaPackage(Package aPackage) {
        this.aPackage = aPackage;
    }

    public List<Service> getServices() {
        return services;
    }

    public void setServices(List<Service> services) {
        this.services = services;
    }
}
