package com.slabblecore.response;

import java.util.List;

public class GetAllAddressByUser {

    List<AddressReponse> addresses;

    public List<AddressReponse> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<AddressReponse> addresses) {
        this.addresses = addresses;
    }
}
