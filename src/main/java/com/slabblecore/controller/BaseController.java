package com.slabblecore.controller;

import com.slabblecore.entity.Package;
import com.slabblecore.entity.Service;
import com.slabblecore.response.GetAllPackageResponse;
import com.slabblecore.response.GetAllService;
import com.slabblecore.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/")
public class BaseController {

    @Autowired
    BaseService baseService;

    @GetMapping("/packages")
    public ResponseEntity<GetAllPackageResponse> getAllPackages(@RequestParam(value = "status",required = false)  String status){
        return baseService.getAllPackages(status);
    }

    @GetMapping("/packages/{package_id}/services")
    public ResponseEntity<GetAllService> getAllServices(@PathVariable("package_id") int packageId){
        return baseService.getAllServiceByPackage(packageId);
    }

    @GetMapping("/services")
    public ResponseEntity<GetAllService> getAllService(){
        return baseService.getAllService();
    }

    @PostMapping("admin/packages")
    public ResponseEntity<Package> createPackage(@Valid @RequestBody Package aPackage){
        return baseService.createPackage(aPackage);
    }

    @PostMapping("admin/services")
    public ResponseEntity<Service> createService(@Valid @RequestBody Service service){
        return baseService.createService(service);
    }

}
