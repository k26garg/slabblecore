package com.slabblecore.controller;

import com.slabblecore.request.SignInRequest;
import com.slabblecore.request.SignUpRequest;
import com.slabblecore.response.BaseResponse;
import com.slabblecore.response.UserResponse;
import com.slabblecore.service.AuthService;
import com.slabblecore.service.OtpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/auth")
public class AuthController {

    @Autowired
    AuthService authService;

    @Autowired
    OtpService otpService;

    @PostMapping("/signup")
    public ResponseEntity<UserResponse> registerUser(@RequestParam(value = "mobileNumber",required = false) String mobileNumber, @Valid @RequestBody SignUpRequest signUpRequest){

        if(mobileNumber==null){

            return authService.registerUserWithMobileNumber(String.valueOf(signUpRequest.getMobileNumber()));
        }
        else{
            return authService.registerUser(mobileNumber,signUpRequest);
        }

    }

    @PostMapping("/signin")
    public ResponseEntity<BaseResponse> loginUser(@Valid @RequestBody SignInRequest signInRequest){
        return authService.loginUser(signInRequest);
    }

    @GetMapping(value = "/generateotp/{mobile_number}")
    public ResponseEntity<Boolean> generateOtp(@PathVariable("mobile_number") String mobileNumber){

        Boolean otp;
        if(!mobileNumber.isEmpty() && mobileNumber.length()==10){
            otp = otpService.generateOtp(mobileNumber);
            return new ResponseEntity<Boolean>(otp,HttpStatus.OK);
        }
        else{
            return new ResponseEntity<Boolean>(HttpStatus.BAD_REQUEST);
        }



    }

    @GetMapping(value = "/validateotp/{mobile_number}/{otp}")
    public ResponseEntity<Boolean> validateOtp(@PathVariable("mobile_number") String mobile_number,@PathVariable("otp") int otp ){

        Boolean isValid = otpService.validateOTP(mobile_number, otp);

        if(isValid){
            return new ResponseEntity<Boolean>(true,HttpStatus.OK);
        }
        else{
            return new ResponseEntity<Boolean>(false,HttpStatus.OK);
        }
    }

}
