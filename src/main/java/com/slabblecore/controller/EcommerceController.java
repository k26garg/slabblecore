package com.slabblecore.controller;

import com.slabblecore.entity.Address;
import com.slabblecore.entity.BaseEntity;
import com.slabblecore.entity.Cart;
import com.slabblecore.entity.Package;
import com.slabblecore.request.CartItemUpdateRequest;
import com.slabblecore.request.CreateAddressRequest;
import com.slabblecore.response.AddressReponse;
import com.slabblecore.response.GetAllAddressByUser;
import com.slabblecore.security.JwtTokenProvider;
import com.slabblecore.service.EcommerceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static org.hibernate.bytecode.BytecodeLogger.LOGGER;

@RestController
@RequestMapping("/api/v1/")
public class EcommerceController{

    @Autowired
    EcommerceService ecommerceService;

    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    @PostMapping("user/address")
    public ResponseEntity<AddressReponse> createAddress(@Valid @RequestBody CreateAddressRequest request){
        long userID = jwtTokenProvider.getLoggedInUserID();
        return ecommerceService.createAddress(userID,request);
    }

    @GetMapping("user/address")
    public ResponseEntity<GetAllAddressByUser> getAllAddressByUser(){
        long userID = jwtTokenProvider.getLoggedInUserID();
        return ecommerceService.getAllAddressByUser(userID);
    }

    @RequestMapping(value = "user/update-cart", method = RequestMethod.POST)
    @ResponseBody
    public Cart updateCart(@Valid @RequestBody CartItemUpdateRequest cartItemUpdateRequest) {

        long userID = jwtTokenProvider.getLoggedInUserID();
        int packageId = cartItemUpdateRequest.getPackageId();

        return ecommerceService.updateCart(userID, packageId,cartItemUpdateRequest.getNoOfBathRoom(),cartItemUpdateRequest.getNoOfBedRoom());

    }


}
